const { Router } = require("express");
const getAnnotations = require("../../utils/getAnnotations");
const router = Router();

router.get("/annotations", function (req, res, next) {
  res.json(getAnnotations());
});

router.get("/annotations/:id", function (req, res, next) {
  const id = req.params.id;
  const { annotations } = getAnnotations();
  const annotationExists = annotations.find((i) => i.resourceId === id);
  if (annotationExists) {
    const annotationResource = annotations.filter((i) => i.resourceId === id);
    res.json(annotationResource);
  } else {
    res.sendStatus(500);
  }
});

module.exports = router;
