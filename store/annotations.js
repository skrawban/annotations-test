export const state = () => ({
  activeAnnotation: null,
  currentFrame: 0,
  list: [],
});
export const getters = {
  currentAnnotations: (state) => {
    return state.list.filter(
      (annotation) => annotation.frameId === state.currentFrame
    );
  },
};
export const mutations = {
  set(state, annotatios) {
    state.list = annotatios;
  },
  setActiveAnnotation(state, annotation) {
    state.activeAnnotation = annotation;
  },
  setFrame(state, frame) {
    state.currentFrame = frame;
  },
  reset(state) {
    state.list = null;
  },
};
